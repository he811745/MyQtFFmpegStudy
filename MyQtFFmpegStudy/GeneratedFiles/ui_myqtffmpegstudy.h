/********************************************************************************
** Form generated from reading UI file 'myqtffmpegstudy.ui'
**
** Created by: Qt User Interface Compiler version 5.6.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYQTFFMPEGSTUDY_H
#define UI_MYQTFFMPEGSTUDY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyQtFFmpegStudyClass
{
public:

    void setupUi(QWidget *MyQtFFmpegStudyClass)
    {
        if (MyQtFFmpegStudyClass->objectName().isEmpty())
            MyQtFFmpegStudyClass->setObjectName(QStringLiteral("MyQtFFmpegStudyClass"));
        MyQtFFmpegStudyClass->resize(600, 400);

        retranslateUi(MyQtFFmpegStudyClass);

        QMetaObject::connectSlotsByName(MyQtFFmpegStudyClass);
    } // setupUi

    void retranslateUi(QWidget *MyQtFFmpegStudyClass)
    {
        MyQtFFmpegStudyClass->setWindowTitle(QApplication::translate("MyQtFFmpegStudyClass", "MyQtFFmpegStudy", 0));
    } // retranslateUi

};

namespace Ui {
    class MyQtFFmpegStudyClass: public Ui_MyQtFFmpegStudyClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYQTFFMPEGSTUDY_H
